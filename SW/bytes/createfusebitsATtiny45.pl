#!/usr/bin/perl -w
#
# createfusebitsATtiny45.pl
#
# Perl script to calculate the lock and fuse bytes for the WS2812 I2S Slave on
# ATtiny45
#
# To use set the parameters in this source file as described in the comments
# below. Result is printed to STDOUT.
#
# Version 1   2020 Oct 25th   jost.brachert@gmx.de
#
# Copyright (C) 2020  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#

use strict;


my $i;

#==============================================================================
# LOCK BITS
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.1 Program And Data Memory Lock Bits

#------------------------------------------------------------------------------
# Lock Bit Settings
#
# Note: Lock bits can only be erased to 1 by Chip Erase command.
#

# lock 0-1  Lock Bits
#           Set exactly one of the elements to one, all others to zero
my @LB = (
    0,  # Flash & EEPROM programming and verification disabled, boot bits & fuses locked
    0,  # Reserved
    0,  # Flash & EEPROM programming disabled, fuses locked
    1,  # No locks enabled
);

# Set to 1 if to be programmed:
my $lock2 = 0;     # Reserved
my $lock3 = 0;     # Reserved
my $lock4 = 0;     # Reserved
my $lock5 = 0;     # Reserved
my $lock6 = 0;     # Reserved
my $lock7 = 0;     # Reserved

#------------------------------------------------------------------------------
# Lock Byte Assembly

    my $locks = 0;

    for ($i=0; $i<@LB; ++$i) {
        last if $LB[$i];
    }
    $locks |= $i;

    $locks |= ( ! $lock2) << 2;
    $locks |= ( ! $lock3) << 3;
    $locks |= ( ! $lock4) << 4;
    $locks |= ( ! $lock5) << 5;
    $locks |= ( ! $lock6) << 6;
    $locks |= ( ! $lock7) << 7;


#==============================================================================
# LOW FUSE BYTE
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.2 Fuse Bits

#------------------------------------------------------------------------------
# Low Fuse Byte Settings
#
#   Displayed bit values here: 0->programmed, 1->erased
#
# - - - - - - - - - - - - - - - - - - - - - SUT CKSEL
# External Clock                                 0000
# High Frequency PLL Clock                       0001
# Internal Calibrated RC Oscillator 8 MHz        0010
# Internal Calibrated RC Oscillator 6.4 MHz      0011 ("ATtiny15 Compat. Mode")
# 128kHz Internal Oscillator                     0100
# Start Up Time (SUT) recommended for
#  BOD enabled                               00
#  Fast rising power                         01
#  Slowly rising power                       10
#  Reserved                                  11
#
# - - - - - - - - - - - - - - - - - - - - - SUT CKSEL
# Reserved                                   xx  0101
#
# - - - - - - - - - - - - - - - - - - - - - SUT CKSEL
# Low-Frequency Crystal Oscillator               0110
# Start Up Time (SUT)
#  Fast rising power or BOD enabled          00
#  Slowly rising power                       01
#  Stable frequency at start-up              10
#  Reserved                                  11
#
# - - - - - - - - - - - - - - - - - - - - - SUT CKSEL
# Reserved                                   xx  0111
#
# - - - - - - - - - - - - - - - - - - - - - SUT CKSEL
# Low Power Crystal Osc 0.4-0.9MHz               100x
# Low Power Crystal Osc 0.9-3.0MHz               101x
# Low Power Crystal Osc 3.0-8.0MHz               110x
# Low Power Crystal Osc 8.0-16.MHz               111x
#
# Ceramic resonator, fast rising power       00     0
# Ceramic resonator, slowly rising power     01     0
# Ceramic resonator, BOD enabled             10     0
# Ceramic resonator, fast rising power       11     0
# Ceramic resonator, slowly rising power     00     1
# Crystal Oscillator, BOD enabled            01     1
# Crystal Oscillator, fast rising power      10     1
# Crystal Oscillator, slowly rising power    11     1

# !! Set bit to 0 if to be programmed
my $SUT   =   0b10;
my $CKSEL = 0b0010;

# Set to 1 if to be programmed:
my $CKOUT  = 0;     # 1: Enable clock output
my $CKDIV8 = 0;     # 1: Divide clock by 8

#------------------------------------------------------------------------------
# Low Fuse Byte Assembly

    my $lfuse = ($SUT << 4) | $CKSEL;
    $lfuse |= ( ! $CKOUT ) << 6;
    $lfuse |= ( ! $CKDIV8) << 7;


#==============================================================================
# HIGH FUSE BYTE
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.2 Fuse Bits

#------------------------------------------------------------------------------
# High Fuse Byte Settings

my @BODLEVEL = (    # Set exactly one of the elements to one, all others to zero:
    0,  # Reserved           # 000 (0 here means programmed, 1 erased)
    0,  # Reserved           # 001
    0,  # Reserved           # 010
    0,  # Reserved           # 011
    0,  # VBOTtyp 4.3V       # 100
    0,  # VBOTtyp 2.7V       # 101
    0,  # VBOTtyp 1.8V       # 110
    1,  # BOD disabled       # 111
);

# Set to 1 if to be programmed:
my $EESAVE  = 0;    # 1: EEPROM memory is preserved through the Chip Erase
my $WDTON   = 0;    # 1: Watchdog Timer always on
my $SPIEN   = 1;    # 1: Enable Serial Program and Data Downloading
my $DWEN    = 0;    # 1: debugWIRE Enable
my $RSTDISBL= 0;    # 1: External Reset Disable

#------------------------------------------------------------------------------
# High Fuse Byte Assembly

    my $hfuse = 0;

    for ($i=0; $i<@BODLEVEL; ++$i) {
        last if $BODLEVEL[$i];
    }
    $hfuse |= $i;

    $hfuse |= ( ! $EESAVE   ) << 3;
    $hfuse |= ( ! $WDTON    ) << 4;
    $hfuse |= ( ! $SPIEN    ) << 5;
    $hfuse |= ( ! $DWEN     ) << 6;
    $hfuse |= ( ! $RSTDISBL ) << 7;


#==============================================================================
# EXTENDED FUSE BYTE
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.2 Fuse Bits

#------------------------------------------------------------------------------
# Extended Fuse Byte Settings

# Set to 1 if to be programmed:
my $SELFPRGEN = 0;
my $efuse1    = 0;     # Reserved
my $efuse2    = 0;     # Reserved
my $efuse3    = 0;     # Reserved
my $efuse4    = 0;     # Reserved
my $efuse5    = 0;     # Reserved
my $efuse6    = 0;     # Reserved
my $efuse7    = 0;     # Reserved

#------------------------------------------------------------------------------
# Extended Fuse Byte Assembly

    my $efuse = 0;

    $efuse |= ( ! $SELFPRGEN ) << 0;
    $efuse |= ( ! $efuse1    ) << 1;
    $efuse |= ( ! $efuse2    ) << 2;
    $efuse |= ( ! $efuse3    ) << 3;
    $efuse |= ( ! $efuse4    ) << 4;
    $efuse |= ( ! $efuse5    ) << 5;
    $efuse |= ( ! $efuse6    ) << 6;
    $efuse |= ( ! $efuse7    ) << 7;


#==============================================================================
# OUTPUT

printf "locks:    %3d    0x%02x    0b%08b\n", $locks, $locks, $locks;
printf "lfuse:    %3d    0x%02x    0b%08b\n", $lfuse, $lfuse, $lfuse;
printf "hfuse:    %3d    0x%02x    0b%08b\n", $hfuse, $hfuse, $hfuse;
printf "efuse:    %3d    0x%02x    0b%08b\n", $efuse, $efuse, $efuse;
