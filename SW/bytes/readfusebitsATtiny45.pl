#!/usr/bin/perl -w
#
# readfusebitsATtiny45.pl
#
# Perl script to get the lock and fuse bits from the lock and fuse bytes of an
# ATtiny45 processor.
#
# For usage info run the script with argument -h.
#
# Version 1.1   2020 Oct 25th   jost.brachert@gmx.de
#
# Copyright (C) 2017-2020  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#

use strict;

#==============================================================================
# Fuse Bytes Descriptors and Descriptions
#

#------------------------------------------------------------------------------
# LOCK BITS
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.1 Program And Data Memory Lock Bits

my @LB = (      # Lock Bits
    'Flash & EEPROM programming and verification disabled, boot bits & fuses locked',
    'reserved',
    'Flash & EEPROM programming disabled, fuses locked',
    'No locks enabled',
);

my @locks = (
    {name=>'LB'    , bit=>0, size=>2, tab=>\@LB  , desc=>'Lock bits'},
    {name=>'lock-2', bit=>2, size=>1,            , desc=>'Reserved'},
    {name=>'lock-3', bit=>3, size=>1,            , desc=>'Reserved'},
    {name=>'lock-4', bit=>4, size=>1,            , desc=>'Reserved'},
    {name=>'lock-5', bit=>5, size=>1,            , desc=>'Reserved'},
    {name=>'lock-6', bit=>6, size=>1,            , desc=>'Reserved'},
    {name=>'lock-7', bit=>7, size=>1,            , desc=>'Reserved'},
);


#------------------------------------------------------------------------------
# LOW FUSE BYTES
#
# Clock Selection, Brown Out Detection, Start Up Time
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   20.2 Fuse Bits

sub cksel_sut {
    my $lfuse = $_[0];

    my $sut   = ($lfuse>>4) & 0x3;
    my $cksel = ($lfuse   ) & 0xF;

    my $ckseldesc = 'Select clock source';
    my $sutdesc   = 'Select start-up time';

    if ($cksel <= 0x4) {
        my @ckselval = (                               # CKSEL
            'External Clock',                          #  0000
            'High Frequency PLL Clock',                #  0001
            'Int. cal. RC Osc 8MHz',                   #  0010
            'Int. cal. RC Osc 6.4MHz,ATtiny15 comp.',  #  0011
            'Int. 128kHz Osc',                         #  0100
        );
        my @sutval = (                                         # SUT
            'BOD enabled',                                     #  00
            'Fast rising power',                               #  01
            'Slowly rising power',                             #  10
            'Reserved',                                        #  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL', $ckselval[$cksel], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'SUT'  , $sutval  [$sut  ], $sutdesc;
    } elsif ($cksel == 0x5) {
        printf "%-10s %-12s [%s]\n", 'CKSEL', 'Reserved', $ckseldesc;
    } elsif ($cksel == 0x6) {
        my @ckselval = (                               # CKSEL
            'LF Crystal Osc',                          #  0110
        );
        my @sutval = (                                         # SUT
            'Fast rising power or BOD enabled',                #  00
            'Slowly rising power',                             #  01
            'Stable frequency at start-up',                    #  10
            'Reserved',                                        #  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL', $ckselval[$cksel-6], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'SUT'  , $sutval  [$sut    ], $sutdesc;
    } elsif ($cksel == 0x7) {
        printf "%-10s %-12s [%s]\n", 'CKSEL', 'Reserved', $ckseldesc;
    } else {
        my @ckselval = (                               # CKSEL
            'Low Power Crystal Osc 0.4-0.9MHz',        #  100x
            'Low Power Crystal Osc 0.9-3.0MHz',        #  101x
            'Low Power Crystal Osc 3.0-8.0MHz',        #  110x
            'Low Power Crystal Osc 8.0-16.MHz',        #  111x
        );
        my @sutval = (                                 #CKSEL0 SUT
            'Ceramic resonator, fast rising power',    #     0  00
            'Ceramic resonator, slowly rising power',  #     0  01
            'Ceramic resonator, BOD enabled',          #     0  10
            'Ceramic resonator, fast rising power',    #     0  11
            'Ceramic resonator, slowly rising power',  #     1  00
            'Crystal Oscillator, BOD enabled',         #     1  01
            'Crystal Oscillator, fast rising power',   #     1  10
            'Crystal Oscillator, slowly rising power', #     1  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL1-3', $ckselval[($cksel>>1)-4], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'CKSEL0/SUT', $sutval[(($cksel&1)<< 2)+$sut], $sutdesc;
    }
}

my @lfuse = (
    {name=>'CKOUT'  , bit=>6, size=>1, desc=>'Enable clock output'},
    {name=>'CKDIV8' , bit=>7, size=>1, desc=>'Divide clock by 8'},
);


#------------------------------------------------------------------------------
# HIGH FUSE BYTE
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   28.2 Fuse Bits

my @BODLEVEL = (
    'Reserved',         # 000
    'Reserved',         # 001
    'Reserved',         # 010
    'Reserved',         # 011
    'VBOTtyp 4.3V',     # 100
    'VBOTtyp 2.7V',     # 101
    'VBOTtyp 1.8V',     # 110
    'BOD disabled',     # 111
);

my @hfuse = (
    {name=>'BODLEVEL', bit=>0, size=>3, tab=>\@BODLEVEL, desc=>'Brown-out Detector trigger level'},
    {name=>'EESAVE'  , bit=>3, size=>1,                  desc=>'EEPROM memory is preserved through the Chip Erase'},
    {name=>'WDTON'   , bit=>4, size=>1,                  desc=>'Watchdog Timer Always On'},
    {name=>'SPIEN'   , bit=>5, size=>1,                  desc=>'Enable Serial Programming and Data Downloading'},
    {name=>'DWEN'    , bit=>6, size=>1,                  desc=>'debugWIRE Enable'},
    {name=>'RSTDISBL', bit=>7, size=>1,                  desc=>'External Reset Disable'},
);


#------------------------------------------------------------------------------
# EXTENDED FUSE BYTE
#
# Brown Out Detection
#
# Atmel 8-Bit AVR Microcontroller ATtiny25/45/85 DATASHEET, Rev. 2586Q-08/13
#   http://www.microchip.com/wwwproducts/en/ATTINY45
#   28.2 Fuse Bits

my @efuse = (
    {name=>'SELFPRGEN', bit=>0, size=>1,                , desc=>'Self-programming enabled'},
    {name=>'efuse-1'  , bit=>1, size=>1,                , desc=>'reserved'},
    {name=>'efuse-2'  , bit=>2, size=>1,                , desc=>'reserved'},
    {name=>'efuse-3'  , bit=>3, size=>1,                , desc=>'reserved'},
    {name=>'efuse-4'  , bit=>4, size=>1,                , desc=>'reserved'},
    {name=>'efuse-5'  , bit=>5, size=>1,                , desc=>'reserved'},
    {name=>'efuse-6'  , bit=>6, size=>1,                , desc=>'reserved'},
    {name=>'efuse-7'  , bit=>7, size=>1,                , desc=>'reserved'},
);



#==============================================================================
# Script
#

#------------------------------------------------------------------------------
# Script Arguments

my $dir = '.';
my $line;

if (defined $ARGV[0]) {
    if (substr($ARGV[0],0,1) eq '-') {
        print "usage: ".__FILE__." [directory]\n";
        print "The script reads the fuse and lock byte from files\n";
        print "locks.txt\nlfuse.txt\nhfuse.txt\nefuse.txt\n";
        exit (0);
    }
    $dir = $ARGV[0];
}


#------------------------------------------------------------------------------
# Analyze Fuse Bytes

readbits ("$dir/locks.txt", "Lock Byte", \@locks);
readbits ("$dir/lfuse.txt", "Low Fuse Byte", \@lfuse, \&cksel_sut);
readbits ("$dir/hfuse.txt", "High Fuse Byte", \@hfuse);
readbits ("$dir/efuse.txt", "Extended Fuse Byte", \@efuse);

print "Legend: '+': programmed (0), '-': not programmed (1)\n";
print "Note that the [comment] refers to the option, not to the current setting\n";





#==============================================================================
# Subroutines

sub readbits {
    my $file  = shift;
    my $label = shift;
    my $desc  = shift;
    my $sub   = shift;

    if ( ! open FILE,$file) {
        print "$label: Could not open $file:\n$!\n\n";
        return;
    }

    my $line = <FILE>;
    chomp $line;
    print "$label: $line\n";
    $line = oct $line;

    for my $h (@{$desc}) {
        my $v = ($line >> $h->{bit}) & ((1 << $h->{size}) - 1);
        if ($h->{size} == 1) {
            unless ($h->{desc} eq 'reserved' and $v) {
                printf "%-10s %-12s [%s]\n", $h->{name}, $v?'-':'+', $h->{desc};
            }
        } else {
            printf "%-10s %-12s [%s]\n", $h->{name}, $h->{tab}->[$v], $h->{desc};
        }
    }

    &{$sub}($line)  if defined $sub;

    print "\n";
}
