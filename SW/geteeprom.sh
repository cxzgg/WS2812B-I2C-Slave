#!/bin/sh

# Shell script to read the EEPROM from an AVR 8 bit device via avrdude

# -P port
# -p part
# -c programmer
# -b baud rate
# -U memory operation
#   :r  read
#   :h  HEX value

avrdude -P/dev/ttyACM0 -pATtiny45 -cstk500v1 -b115200 -Ueeprom:r:eeprom.txt:h

echo "EEPROM:"
cat eeprom.txt
