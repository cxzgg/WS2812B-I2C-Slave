//##############################################################################
//
// Program to drive 16 WS2812B controlled LEDs as an I2C slave
//
// Version 1   2020 Oct 25th   jost.brachert@gmx.de
//
// The program is intended to be run on a Microchip ATtiny45.
// The fuses must be set to internal 8 MHz RC clock.
//
// The program is a substantial extension of the neopixel_i2c_slave of
// Brian Starkey, see https://github.com/usedbytes/neopixel_i2c.
//
// It uses a slightly modified version of the I2C slave library of Brian Starkey
// https://github.com/usedbytes/usi_i2c_slave.
// For details about the modifications see i2c/i2c_machine.c.
//
// It uses also a slightly modified version of the light_ws2812 V2.3 library of
// Tim (cpldcpu@gmail.com) as of 2016-Feb-20.
// https://github.com/usedbytes/light_ws2812/tree/b4dcd56029ee2d20913c29acfc43fc05904f9271
// For details about the modifications see ws2812/light_ws2812.c.
//
//------------------------------------------------------------------------------
// Copyright (C) 2020  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of version 2 of the GNU General Public License as published by the
// Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//

#include <stdint.h>             // uint8_t
#include <util/delay.h>         // _delay_ms()
#include <avr/io.h>             // DDRx,PORTx,PINx,TCCRxx,TIMSKx,...
#include <avr/interrupt.h>      // sei(),cli(),ISR,TIMER0_COMPA_vect
#include <avr/eeprom.h>         //
#include <avr/pgmspace.h>       // PROGMEM,pgm_read_byte()
#include <avr/wdt.h>            // wdt_reset()
extern "C" {
#include "./light_ws2812.h"     // ws2812*,cRGB
#include "i2c/i2c_machine.h"    // i2c_*
#include "./i2c_slave_defs.h"   // I2C_N_REG,I2C_N_GLB_REG,REG_CTRL,CTRL_RST,
                                // CTRL_GLB,REG_GLB_G,REG_GLB_R,REG_GLB_B,N_LEDS
}


// Decay of brightness for swirly function
static const bool  swirlySlowFade = false;    // false or true

// Bitmask for time divider defining the swirly speed
// 0x00 or 0x01 or 0x3 or 0x7 ...
static const uint16_t  swirlyTimeDivider = 0x00;


#ifndef __AVR_ATtiny45__        // Verify correct library for part, see avr/io.h
#error Program is for ATtiny45 only
#endif


//==============================================================================
// Global Parameters

//------------------------------------------------------------------------------
// Timing parameters

#ifdef F_CPU
#   if F_CPU != 8000000
#       error F_CPU defined != 8000000: check setup code
#   endif
#else
#   error F_CPU not defined
#endif

// Timer0 clock divider
// NOTE: Change set up code in setTimer() when changing this value
static const double   timer0ClkDiv = 1024;

// Time in seconds between calls of Timer0 interrupts and thus between updates
// of variable "time". Value selected such that the timer0Top gets an integer
// result less than 256.
static const double   timeSlice = 0.032;

static const uint8_t  timer0Top = (uint8_t)(F_CPU/timer0ClkDiv*timeSlice+0.5);


//------------------------------------------------------------------------------
// I/O and EEPROM parameters

static const int       optionPin = 4;   // PB4 Pin 3  Option Select Pin

                                        // Address of I2C slave addr. in EEPROM
static const uint8_t  *i2cSlaveAddrEepAddr = reinterpret_cast<uint8_t*>(1);


//------------------------------------------------------------------------------
// I2C variables

static const uint8_t   initColor[3] PROGMEM = { 0x00, 0xFF, 0x00 };  // G,R,B

volatile uint8_t  i2c_reg[I2C_N_REG];   // Communication array for I2C library
uint8_t           i2c_slaveAddr;        // I2C slave address used in I2C library


//------------------------------------------------------------------------------
// Timer interface

static volatile uint16_t  time;         // Counter of timer interrupts
                                        // Time in increments of timeSlice


//==============================================================================

static void  setTimers (void);
static void  setLedsGlobal (void);
static void  updateLeds (void);
static void  doReset (void);
static void  swirly (void);




//##############################################################################
// Main Program

int  main (void)
{
    cli();                                  // Disable interrupts

    //==========================================================================
    // Watchdog
    //
    // ATtiny25/45/85 [DATASHEET] 2586Q–AVR–08/2013
    // 8.5.2 WDTCR - Watchdog Timer Control Register:
    //   Note:
    //   If the watchdog timer is not going to be used in the application,
    //   it is important to go through a watchdog disable procedure in the
    //   initialization of the device. If the Watchdog is accidentally enabled,
    //   for example by a runaway pointer or brown-out condition, the device
    //   will be reset, which in turn will lead to a new watchdog reset. To
    //   avoid this situation, the application software should always clear the
    //   WDRF flag and the WDE control bit in the initialization routine.
    //
    // Clear WD System Reset Flag to recover savely from a (possibly
    // unintentional) Watchdog System Reset.
    //
    // NOTE: This is rather late in the program to do that, since before the
    //       call to main() there are other initializations like zeroing of the
    //       bss segments.
    //       If it turns out that it is too late here then the watchdog code
    //       can be put to an earlier state of the program as suggested by the
    //       avr-libc FAQ website:
    //       http://www.nongnu.org/avr-libc/user-manual/FAQ.html#faq_startup

    // Trigger the watchdog to clear an accidentally pending interrupt
    wdt_reset();

    // Reset Watchdog System Reset Flag to avoid an eternal reset loop by
    // accidentally enabled watchdog
    MCUSR = 0;

    // Initiate changing the WDT parameters by setting Watchdog Change
    // Enable (WDCE) and Watchdog System Reset Enable (WDE)
    WDTCR = (1<<WDCE) | (1<<WDE);
    // 4 cycles from here to set the new WDT parameters ...

    // Stop Watchdog: disable WD System Reset (WDE) and WD interrupt (WDIE)
    WDTCR = 0;


    //==========================================================================
    // Initialize microprocessor

    //--------------------------------------------------------------------------
    // Switch off unnecessary peripherals

    PRR = (1<<PRTIM1)|(1<<PRADC);       // Switch off Timer1 and ADC
    ACSR = (1<<ACD);                    // Switch off Analog Comparator
    DIDR0 |= (1<<AIN1D);                // Switch off digital input buffer on
                                        //   AIN1D, i.e. PB1, pin 6

    //--------------------------------------------------------------------------
    // Initialize I/O pins
    // Pin symbols see /opt/cross/avr/avr/include/avr/iotnx5.h

    // NOTE that the WS2812 output pin is initialized in the WS2812 driver

    PORTB = (1<<optionPin);             // Invoke internal pullup at option pin

    //--------------------------------------------------------------------------
    // Set up I2C interface

    i2c_slaveAddr = I2C_SLAVE_ADDR;

    // If option pin is low ...
    if ( ! (PINB & (1<<optionPin))) {
        // ... read I2C address from EEPROM
        uint8_t  i2cAddrEeprom = eeprom_read_byte (i2cSlaveAddrEepAddr);

        // The the address is in the appropriate range then use as slave address
        if (i2cAddrEeprom > 0 && i2cAddrEeprom <= 0x7F) {
            i2c_slaveAddr = i2cAddrEeprom;
        }
    }
    i2c_init ();

    //--------------------------------------------------------------------------
    // Set up timers
    setTimers ();

    //--------------------------------------------------------------------------
    // Initialize LED values
    doReset ();

    sei();                                 // Re-enable interrupts again


    //==========================================================================
    // Endless Main Loop

    for (;;) {
        // Check if an I2C message has been received
        if (i2c_check_stop()) {
            if (REG_CTRL == CTRL_RST) {
                doReset ();
            } else if (REG_CTRL == CTRL_GLB) {
                REG_CTRL = CTRL_GLB;       // Reset the other possibly set flags
                setLedsGlobal ();
            }
            // NOTE that REG_CTRL might also be set to CTRL_SWIRLY.
            // That is handled in the Timer0 Compare Match A interrupt.
            else {                         // NOLINT
                updateLeds ();
            }
        }
    }

    return 0;
}



//==============================================================================
// setLedsGlobal
//
// Set all LEDs to the same color value
//

static inline void  setLedsGlobal (void)
{
    ws2812_setleds_constant ((struct cRGB *)&REG_GLB_G, N_LEDS);
}



//==============================================================================
// updateLeds
//
// Send individual LEDs values as array
//
// NOTE: This function is called from a regular timer interrupt if the swirly
//       function is active.
//

static inline void  updateLeds (void)
{
    // The (uint8_t*) cast is necessary because the the function expects a
    // non-volatile argument but the values are volatile.
    // This is ok since only single bytes of the array are used so that there
    // is no risk of inconsistent values and there is no risk that the compiler
    // will optimize the array away.
    ws2812_sendarray (const_cast<uint8_t*>(i2c_reg) + I2C_N_GLB_REG, N_LEDS*3);
}



//==============================================================================
// doReset
//
// Send an initial color value to all LEDs.
// Initialize the global LED color value with the default value (that will be
// used if an I2C message is received with CTRL_GLB bit set in REG_CTRL but
// without accompanying color values).
//

static void  doReset (void)
{
    // Set all LEDs to a certain color
    REG_GLB_G = 0;
    REG_GLB_R = 0;
    REG_GLB_B = 50;
    ws2812_setleds_constant ((struct cRGB *)&REG_GLB_G, N_LEDS);

    // Reinitialize the global LED settings to the default value
    // These will be used if CTRL_GLB is set in REG_CTRL but no global LED
    // values have been received.
    REG_GLB_G = pgm_read_byte(initColor);
    REG_GLB_R = pgm_read_byte(initColor + 1);
    REG_GLB_B = pgm_read_byte(initColor + 2);
    REG_CTRL = 0;

    // Reset the registers or we'll just go back to the old values!
    {
        uint8_t  i = N_LEDS * 3;
        volatile uint8_t *p = i2c_reg + I2C_N_GLB_REG;
        while (i--) {
            *p = 0;
            ++p;
        }
    }
}



//==============================================================================
// Timer Set Up
//

static void setTimers () {
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Timer0 is used to define the system time

    // NOTE: This value should be verified if in TCCR0B the clock divider bits
    //       are changed.
    OCR0A = timer0Top;
    OCR0B = 0;                      // Register not used

    // TCCR0A is set to normal port operation and together with TCCR0B to
    // Clear Timer on Compare Match (CTC) mode, TOP = OCR0A
    TCCR0A = (1<<WGM01);

    // Set timer mode together with TCCR0A, see the comment there, and
    // set clock source and divider.
    // NOTE: Adapt timer0ClkDiv (and timer0Top) when changing the clock divider
    //       bits.
    TCCR0B = (1<<CS02)|(1<<CS00);   // Clock source clkI/O, divider 1024

    TIMSK = (1<<OCIE0A);            // Enable Timer0 Compare Match A Interrupt
}



//##############################################################################
// Interrupt Service Routines

// See also: /opt/cross/avr/avr/include/avr/iotnx5.h

//==============================================================================
// Timer 0 Compare Match A Interrupt

ISR(TIMER0_COMPA_vect)              // cppcheck-suppress unusedFunction
{
    ++time;                         // Increment global system time
    if (REG_CTRL == CTRL_SWIRLY) {
        if ((time & swirlyTimeDivider)==0)  swirly ();
    }
}



//==============================================================================
// swirly
//
// Realize a spinning impression on an LED chain arranged in a circle
//
// NOTE: This function is called from a regular timer interrupt
//

static void  swirly (void)
{
    static   uint8_t   iLed;        // Index of current LED
    volatile uint8_t  *p1;          // Ptr into the LED array
    volatile uint8_t  *p2;          // "
                                    // Start and end of the LED array in i2c_reg
    volatile uint8_t *const  pS = &i2c_reg[I2C_N_GLB_REG];
    volatile uint8_t *const  pE = &i2c_reg[I2C_N_GLB_REG + N_LEDS*3];

    uint8_t  g = REG_GLB_G;         // Default global green value
    uint8_t  r = REG_GLB_R;         //    "           red
    uint8_t  b = REG_GLB_B;         //    "           blue

    uint8_t  i;

    p2 =
    p1 = &i2c_reg[I2C_N_GLB_REG + iLed*3];
    *(p2++) = g;
    *(p2++) = r;
    *(p2++) = b;
    if (p2 >= pE)  p2 = pS;

    for (i=0; i<N_LEDS; ++i) {
        if (swirlySlowFade) {
            *(p2++) = *(p1++)*3u/4u;        // Green
            *(p2++) = *(p1++)*3u/4u;        // Red
            *(p2++) = *(p1++)*3u/4u;        // Blue
        } else {
            *(p2++) = *(p1++)/2u;           // Green
            *(p2++) = *(p1++)/2u;           // Red
            *(p2++) = *(p1++)/2u;           // Blue
        }
        if (p1 >= pE)  p1 = pS;
        if (p2 >= pE)  p2 = pS;
    }

    iLed = (iLed==0) ? N_LEDS-1 : iLed-1;

    updateLeds ();                          // Send values to LEDs
}
