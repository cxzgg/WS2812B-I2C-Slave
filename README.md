# WS2812B I²C Slave

This project describes a device that accepts commands via I²C to drive up to 16
RGB color LEDs individually via a WS2812B bus.

Version 1 2020-Oct-25th jost.brachert@gmx.de


## Features

 * WS2812B is a bus system enabling individual control of single color LEDs in
   LED chains.
 * The device can control up to 16 RGB color LEDs individually in WS2812B LED
   chains.
 * The color and brightness values for each LED are received via an I²C bus.
 * The I²C slave address is fixed to 64 (40Hex) in the firmware.
 * By checking an option pin at startup can the I²C slave address be set to a
   value in EEPROM.
 * The I²C slave address in EEPROM is changeable via ISP interface.
 * New firmware can be loaded into the device via ISP interface.
 * The output pin to the WS2812B LEDs is short-circuit proof.

Operating modes:

 * In *normal* mode, each LED is individually driven based on the value in its
   color registers.
 * In *global* mode, all LEDs are driven to the same value, based on the values
   in the global color registers.
 * In *swirly* mode, the LEDs are driven to get a color swirl if arranged in a
   circle. The color is defined by the values in the global color registers.

The operating mode is defined by setting an according value in the control
register.


## Software

The project contains the software as **C and C++ source files** as well as the
resulting [**Intel hex file**](SW/build/ws2812Bi2cslave.hex) for firmware upload
in sub-directory [**SW/src**](SW/src) and [**SW/build**](SW/build) respectively.

The software of the device is inspired by and a substantial extension of the
**neopixel\_i2c\_slave** of Brian Starkey
<https://github.com/usedbytes/neopixel_i2c>.

It uses a slightly modified version of the **I2C slave library** of Brian Starkey
<https://github.com/usedbytes/usi_i2c_slave>, see sub-directory
[**SW/src/i2c**](SW/src/i2c). For details about the modifications see the
comments in file [SW/src/i2c/i2c\_machine.c](SW/src/i2c/i2c_machine.c).

It uses also a slightly modified part of the **light\_ws2812 V2.3** library of
Tim (cpldcpu@gmail.com) as of 2016-Feb-20
<https://github.com/usedbytes/light_ws2812/tree/b4dcd56029ee2d20913c29acfc43fc05904f9271>,
see sub-directory [**SW/src/ws2812**](SW/src/ws2812). For details about the
modifications see the according comments in
[SW/src/ws2812/light\_ws2812.c](SW/src/ws2812/light_ws2812.c).

Files [SW/src/i2c\_slave\_defs.h](SW/src/i2c_slave_defs.h) and
[SW/src/ws2812\_config.h](SW/src/ws2812_config.h) are include files to configure
those libraries.

A [**Makefile**](SW/Makefile) controlling the build and upload process can be
found in directory [**SW**](SW), type 'make help' in that directory for details.
It calls the **avr-gnu C and C++ compiler** to build the SW and
[**avrdude**](http://savannah.nongnu.org/projects/avrdude) to upload the SW to
the device.

Note that you have to adapt the parameters in the Makefile to the programmer and
programmer port you use.

Shell script template [**geteeprom.sh**](SW/geteeprom.sh) is provided to
download the **EEPROM** contents of the microcontroller using **avrdude**.
The optional I²C slave address is located at address 1 in the EEPROM (the 2nd
byte, counting from 0). A new optional I²C slave address can be *uploaded* to
the device using the Makefile.

Note that you have to adapt the programmer and programmer port parameters in
geteeprom.sh and the Makefile to your needs.

Perl scripts to create fuse and lock bytes and to interprete existing lock and
fuse bytes are provided in sub-directory [**SW/bytes**](SW/bytes).


## Hardware Description

The project uses an AVR **ATtiny45** microcontroller with fuses set to internal
8 MHz RC oscillator.

*Schematic* and *PCB specification* are available as a **Fritzing 0.9.3**
project in sub-directory [**Fritzing**](Fritzing) and as according exports in
sub-directory [**Fritzing/exports**](Fritzing/exports).

Directory [**Fritzing/parts**](Fritzing/parts) contains the parts used in the
Fritzing project but not available in the standard Fritzing part library.


## Analysis, Requirements, Design

The file [**AnaReqDes.txt**](AnaReqDes.txt) contains an analysis and the
requirements of the project, as well as the results of the design decisions made
in the project.

File [**AnaReqDes.html**](AnaReqDes.html) contains the same converted to HTML
using the script [**mkhtml.pl**](https://codeberg.org/cxzgg/mkhtml.pl.git).


## Documentation

A detailed user manual is provided in sub-directory [**doc**](doc) as
**docbook 5.0** source ([doc/WS2812B-I2CSlave.xml](doc/WS2812B-I2CSlave.xml))
and as compiled **PDF**, **HTML**, and **text** results in English and German
language.

The images of PCB and the schematic used in the documentation are manually
modified versions of the Fritzing exports.

A [Makefile](doc/Makefile) to control the compilation of the documention can be
found in the same directory, type 'make help' in that directory for details.

The project uses _xsltproc (libxslt)_
[http://www.xmlsoft.org/](http://www.xmlsoft.org/)
and Perl to create the HTML documention and intermediate XML files. It uses
_w3m_
[https://sourceforge.net/projects/w3m/](https://sourceforge.net/projects/w3m/)
to create the text version of the documention and _xmlgraphics fop_
[https://xmlgraphics.apache.org/fop/](https://xmlgraphics.apache.org/fop/)
to create the PDF version.


## Test

A test program as
[WS2812-I2CSlave-TestMaster.ino](WS2812-I2CSlave-TestMaster/WS2812-I2CSlave-TestMaster.ino)
for an Arduino Uno is provided in sub-directory
[**WS2812-I2CSlave-TestMaster**](WS2812-I2CSlave-TestMaster). Set an appropriate
```#define``` from 0 to 1 to activate a certain test.

This program uses the Arduino Wire library to implement the I²C bus.

Note, however, that test of transfers of messages with a size of greater than
32 bytes is not possible with this library. File
[WS2812-I2CSlave-TestMaster.ino](WS2812-I2CSlave-TestMaster/WS2812-I2CSlave-TestMaster.ino)
contains a description how to modify the Wire library so that those tests can be
executed.

Take care that the include file i2c\_slave\_defs.h in that directory containing
the configuration of the I2C library is the same as for the firmware source in
directory SW.


## Static Verification

The Makefile contains configurations and targets to call
[**cppcheck**](https://cppcheck.sourceforge.io/) and
[**cpplint**](https://github.com/google/styleguide/tree/gh-pages/cpplint).

Run the checks with '``` make cppcheck ```' or '``` make cpplint ```'
respectively.

The result of the cpplint check is available in file
[SW/cpplint.err](SW/cpplint.err).

The result of the cppcheck check is available in file
[SW/cppcheck.err](SW/cppcheck.err). Note that the cppcheck result contains
highlighting for ANSI terminal. Therefore view it on the command line with
'```cat cppcheck.err```'.


## License

Software:
  GNU General Public License Version 2, see file SW/src/COPYING or
  <https://www.gnu.org/licenses/gpl-2.0>.

Documentation and Fritzing project:
  Creative Commons Attribution-NonCommercial-ShareAlike 4.0 Unported License:
  <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
