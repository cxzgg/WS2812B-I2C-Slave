<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d"
                version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="yes"
            doctype-public="-//OASIS//DTD DocBook XML V5.0//EN"
            doctype-system="/usr/share/xml/docbook/schema/dtd/5.0/docbook.dtd"/>


<!-- ====================================================================== -->
<!-- Exchange xml:id attributes by remap attributes with the same value     -->
<!-- to prevent from error message "validity error : ID already defined"    -->
<!-- in ende2.xsl after doubling the <part> tag for the second language in  -->
<!-- the following template.                                                -->
<!-- The remap attributes must be re-changed to xml:id attributes           -->
<!-- afterwards in ende2.xsl again.                                         -->

<xsl:template match="@xml:id">
  <xsl:attribute name="remap">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>


<!--========================================================================-->
<!-- Double the <part> section, one with xml:lang="en", one with            -->
<!-- xml:lang="de"                                                          -->

<xsl:template match="d:part">
  <part xml:lang="en">
    <xsl:apply-templates/>
  </part>
  <part xml:lang="de">
    <xsl:apply-templates/>
  </part>
</xsl:template>


<!-- ====================================================================== -->
<!-- Default template: Just copy input to output                            -->

<xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>
