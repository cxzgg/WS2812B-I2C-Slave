<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="1.0">

<xsl:import href="fo-base.xsl"/>

<!-- TODO: remove empty part title page -->


<!--========================================================================-->
<!-- Inhibit output of part titlepage with the language title in            -->
<!-- documents with one language (and thus one part) only                   -->

<xsl:template name="part.titlepage.recto"/>
<xsl:template name="part.titlepage"/>


<!--========================================================================-->
<!-- Remove tags with inappropriate language                                -->

<!-- Not possible here because some XSL processors check id attributes      -->
<!-- despite the fact that nodes are removed.  Therefore moved to separate  -->
<!-- XSL script rmlang.xsl.                                                 -->

<!--xsl:template match="*[@xml:lang!=$l10n.gentext.default.language]"/-->


</xsl:stylesheet>
