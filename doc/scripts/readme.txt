In the docbook source file <title> and <variablelist> tags must not have xml:id
attributes. These attributes will be removed by fop.

However, xml:id attributes in <phrase> tags within <title> tags are allowed.
