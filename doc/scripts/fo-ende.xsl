<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="1.0">

<xsl:import href="fo-base.xsl"/>


<!-- Parameter default.language set to "de" to get an acceptable compromise -->
<!-- for hyphenation                                                        -->
<xsl:param name="l10n.gentext.default.language">de</xsl:param>

<xsl:param name="index.on.type">1</xsl:param>

<!--xsl:param name="header.rule" select="0"/-->
<!--xsl:param name="footer.rule" select="0"/-->


<!--========================================================================-->
<!-- To have the language (i.e. part) in header                             -->

<xsl:template name="header.content">
  <xsl:param name="pageclass" select="''"/>
  <xsl:param name="sequence" select="''"/>
  <xsl:param name="position" select="''"/>
  <xsl:param name="gentext-key" select="''"/>

  <fo:block>
    <!-- sequence can be odd, even, first, blank -->
    <!-- position can be left, center, right -->
    <xsl:if test="$position='center' and $pageclass!='titlepage'">
      <xsl:value-of select="ancestor-or-self::d:part/child::d:title"/>
    </xsl:if>
  </fo:block>
</xsl:template>

</xsl:stylesheet>
