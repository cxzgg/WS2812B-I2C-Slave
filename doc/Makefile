################################################################################
# Makefile for WS2812B IC2 Slave Documentation
#
# Version 1   25-Oct-2020
#
# Copyright (C) 2020  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#
#-------------------------------------------------------------------------------
# For info on how to call see help target
#

prj = WS2812B-I2CSlave

FOP = fop

images = images/WS2812BI2CSlaveSystem.svg\
         images/AlternativeSupply.svg\
         images/2PinConnector.svg\
         images/3PinConnector.svg\
         images/ISPConnector.svg\
         images/WS2812B-I2C-Slave_schemMod.svg\
         images/WS2812B-I2C-Slave_pcbMod.svg

scr = scripts

.PHONY: all html text fo pdf clean distclean verify help

all:
	$(MAKE) pdf lang=ende
	$(MAKE) pdf lang=en
	$(MAKE) pdf lang=de
	$(MAKE) html lang=en
	$(MAKE) html lang=de
	$(MAKE) text lang=en
	$(MAKE) text lang=de


# Shortcuts for easy access

html text fo pdf:
ifeq ($(origin lang), undefined)
	@echo "call with option lang=en or lang=de, e.g.:"
	@echo "    make $@ lang=en"
	@exit 1
endif
	$(MAKE) $@-$(lang)


################################################################################
# HTML Output

# HTML Images
htmlimages = $(images:%=html/%)
$(htmlimages): html/images/%: images/%
	mkdir -p $(dir $@)
	cp $< $@

# Web page as one big file
html-$(lang): html/$(prj)-$(lang).html
html/$(prj)-$(lang).html: $(prj).xml $(htmlimages)\
	                  $(patsubst %,$(scr)/%, rmlang.xsl html.xsl base.xsl)
	@# Remove inappropriate language elements
	@# Note: done here because not possible in html.xsl see comment there
	xsltproc --nonet --xinclude --stringparam language $(lang)\
	         -o $<~ $(scr)/rmlang.xsl $<
	cd html; xsltproc --path .. --nonet --xinclude -o $(notdir $@)\
	         --stringparam l10n.gentext.default.language $(lang) ../$(scr)/html.xsl $<~

# Output as one big text file in UTF-8 format
text-$(lang): $(prj)-$(lang).txt
$(prj)-$(lang).txt: html/$(prj)-$(lang).html $(scr)/html-rmv-toc-idx.xsl
	xsltproc --html --nonet $(scr)/html-rmv-toc-idx.xsl $< \
	 | w3m -O UTF-8 -T text/html -dump > $@


################################################################################
# Book Output

#	-param fop1.extensions 1 required to prevent from error message
#	  "Flow 'xsl-region-body' does not map to the region-body in page-master 'blank'"
#	  at double sided printing.
#	Use additional option -r for relaxed validity checking if necessary.
#	Note that a proper font configuration is required in fop.conf.
pdf-$(lang): $(prj)-$(lang).pdf
ifeq ($(lang), ende)
  $(prj)-$(lang).fo.xml: $(prj).xml\
	              $(patsubst %,$(scr)/%,ende1.xsl ende2.xsl ende3.xsl fo-ende.xsl fo-base.xsl base.xsl)
	perl -wp -e "s|&minus;|&#x2212;|g;s|&ldquo;|&#x201C;|g;s|&rdquo;|&#x201D;|g;s|&mdash;|&#x2014;|g;s|&copy;|&#xA9;|g;s|&nbsp;|&#xA0;|g;" $< >$<~~
	@# Exchange xml:id attributes by remap attributes to prevent from error message
	@# "validity error : ID already defined"
	@# Double <part> or <chapter> tag for the second language
	xsltproc --novalid --nonet --xinclude --stringparam fop1.extensions 1 -o $<~ $(scr)/ende1.xsl $<~~
	@# Remove inappropriate language paragraphs from <part> or <chapter> tags
	xsltproc --novalid --nonet --stringparam fop1.extensions 1 -o $<~~ $(scr)/ende2.xsl $<~
	@# Re-change remap attributes to xml:id attributes
	xsltproc --novalid --nonet --stringparam fop1.extensions 1 -o $<~ $(scr)/ende3.xsl $<~~
	@# Convert to fo.xml
	xsltproc --novalid --nonet --stringparam fop1.extensions 1 -o $@ $(scr)/fo-ende.xsl $<~
  $(prj)-$(lang).pdf: $(prj)-$(lang).fo.xml fop.conf $(images)
	$(FOP) -c fop.conf -fo $< -pdf $@
else
  $(prj)-$(lang).fo.xml: $(prj).xml\
	              $(patsubst %,$(scr)/%, rmlang.xsl fo.xsl fo-base.xsl base.xsl)
	@# Remove inappropriate language elements
	xsltproc --nonet --xinclude --stringparam language $(lang)\
	         --stringparam fop1.extensions 1 -o $<~ $(scr)/rmlang.xsl $<
	@# Convert to fo.xml
	xsltproc --nonet --stringparam fop1.extensions 1 --stringparam l10n.gentext.default.language $(lang)\
	         -o $@ $(scr)/fo.xsl $<~
  $(prj)-$(lang).pdf: $(prj)-$(lang).fo.xml fop.conf $(images)
	$(FOP) -c fop.conf -fo $< -pdf $@
endif

$(prj)-$(lang).dvi: $(prj)-$(lang).fo.xml
	xmltex $(prj)-$(lang).fo.xml

# Verify DocBook xml source ----------------------------------------------------
verify: $(prj).xml
	xmllint --noout --valid --xinclude $<


# Makefile Dependencies --------------------------------------------------------
$(htmlimages)\
html/$(prj)-$(lang).html \
$(prj)-$(lang).txt \
$(prj)-$(lang).dvi \
$(prj)-$(lang).pdf \
$(prj)-$(lang).fo.xml: Makefile


# Clean Targets ----------------------------------------------------------------
clean:
	-rm -vf $(prj)-en.fo.xml $(prj)-de.fo.xml $(prj)-ende.fo.xml
	-rm -vf $(prj)-en.fo.log $(prj)-de.fo.log $(prj)-ende.fo.log missfonts.log
	-rm -vf $(prj)-en.dvi $(prj)-de.dvi $(prj)-ende.dvi
	-rm -vf $(prj).xml~ $(prj).xml~~

distclean: clean
	-rm -vf $(prj)-ende.pdf $(prj)-en.pdf $(prj)-de.pdf
	-rm -vf html/$(prj)-en.html html/$(prj)-de.html
	-rm -vf $(prj)-en.txt $(prj)-de.txt
	-rm -vf $(htmlimages)
	-rm -vd html/images


help:
	@echo "type 'make html lang=en' to get the html output in English"
	@echo "type 'make html lang=de' to get the html output in German"
	@echo
	@echo "type 'make pdf lang=en' to get the pdf output of the English version"
	@echo "type 'make pdf lang=de' to get the pdf output of the German version"
	@echo "type 'make pdf lang=ende' to get the pdf output with an English and a German part"
	@echo
	@echo "type 'make text lang=en' to get the output in English as Unicode UTF-8 text file"
	@echo "type 'make text lang=de' to get the html output in German as Unicode UTF-8 text file"
	@echo
	@echo "type 'make all' to get all html, txt, and pdf result files"
	@echo
	@echo "type 'make clean' to remove all intermediate files"
	@echo "type 'make distclean' to remove all maked files"
	@echo
	@echo "type 'make help' to get this info"
