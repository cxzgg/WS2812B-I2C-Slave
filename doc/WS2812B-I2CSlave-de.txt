WS2812B I²C Slave


Bedienungsanleitung
Version 1

Copyright ©2020 Jost Brachert, jost.brachert@gmx.de

Alle Rechte vorbehalten. Dieses Dokument ist lizensiert unter einer Creative
Commons Nicht-kommerziell – Weitergabe unter gleichen Bedingungen Lizenz https:
//creativecommons.org/licenses/by-nc-sa/4.0/deed.de

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

Haftungsausschluss

Dies ist kein kommerzielles oder industrielles Gerät oder Design. Es wurde
ausschließlich für experimentelle Zwecke erstellt und ist nicht für
professionellen oder kommerziellen Einsatz vorgesehen.

Der Schöpfer des Geräts ist nicht haftbar für jegliche durch das Gerät oder das
Versagen des Geräts verursachte Schäden, einschließlich Gewinneinbußen und
unterbliebener Einsparungen sowie besonderer, indirekter oder Folgeschäden. Des
Weiteren ist der Schöpfer des Geräts nicht haftbar gegenüber Rechtsansprüchen
Dritter oder durch Sie im Namen Dritter angemeldeten Forderungen.

Dieser Haftungsausschluss gilt unabhängig davon, ob Schäden gerichtlich
verfolgt oder Schadensersatzansprüche aufgrund unerlaubter Handlungen
(einschließlich Fahrlässigkeit und Gefährdungshaftung) oder aufgrund
vertraglicher oder sonstiger Ansprüche gestellt werden, und kann von niemandem
aufgehoben oder verändert werden. Dieser Haftungsausschluss ist auch dann
gültig, wenn Sie den Schöpfer des Geräts auf die Möglichkeit solcher Schäden
aufmerksam gemacht haben.

Eigenschaften

Mikrocontrollerbasiertes Gerät um WS2812B-RGB-LED-Pixel anzusteuern. Kommandos
werden über I²C übermittelt.

  ● WS2812B ist ein Bussystem, um in LED-Ketten einzelne Farb-LEDs individuell
    anzusteuern.

  ● Das Gerät kann bis zu 16 RGB-Farb-LEDs in einer WS2812B-LED-Kette
    individuell steuern.

  ● Die Farb- und Helligkeitseinstellungen für jede LED werden über einen
    I²C-Bus empfangen.

  ● Der Ausgangs-Pin zu den WS2812B LEDs ist kurzschlussfest.

  ● Die I²C-Slave-Adresse ist fest eingestellt auf 64 (40Hex).

  ● Über den Zustand eines Options-Jumpers beim Einschalten kann die
    I²C-Slave-Adresse auf einem Wert im EEPROM gesetzt werden.

  ● Die I²C-Slave-Adresse im EEPROM ist änderbar über eine ISP-Schnittstelle.

  ● Neue Firmware kann über eine ISP-Schnittstelle in das Gerät geladen werden.

  ● Das Gerät kann mit Spannungen von 3.3 V bis 5.3 V versorgt werden.

  ● Anschlüsse: 6-mm-Stiftleisten mit 2,54-mm-Raster

Funktionsbeschreibung

Der WS2812B-I²C-Slave ist ein Gerät zur Steuerung von bis zu 16 Dreifarb-LEDs.
Es kann Farbe und Helligkeit jeder einzelnen LED individuell einstellen. Die
Farbwerte werden über einen WS2812B-Bus and die LEDs gesendet, siehe Bild 1.

Der WS2812B-Bus [RfWS2812BDS] ist ein Ein-Draht-Punkt-zu-Punkt-Bus. Pin D[OUT]
des WS2812B-I²C-Slave ist verbunden mit D[IN] der ersten LED. Diese LED
entnimmt die für sich bestimmte Farbinformation und sendet den Rest über ihren
D[OUT]-Pin zur nächsten LED und so weiter.

WS2812B-LEDs können als Einzel-LEDs bezogen werden oder fertig in Form von
Streifen, Kreisen oder anderen Anordnungen.

[WS2812BI2C]

Bild 1: WS2812B-I²C-Slave Systembild

Das Gerät bezieht seine Kommandos zur Steuerung der LEDs von einem
Host-Controller über einen I²C-Bus (Wikipedia: I²C [RfWPI2C_de]).

Der I²C-Bus ist ein Bus zur Verbindung mehrerer elektronischer Einheiten mit
zwei Drähten plus einer Masseverbindung. Es wird daher auch als
„Zwei-Draht-Bus” oder Two-Wire-Bus bezeichnet.

Ein I²C-Bus verbindet alle angeschlossenen Einheiten mit denselben zwei
Drähten. Ein Bus-Master organisiert die Kommunikation mit mehreren Slaves. Er
sendet Botschaften an einen Slave oder veranlasst, dass ein Slave eine
Botschaft sendet. Jeder Slave muss eine spezifische Slave-Adresse haben, die
natürlich dem Master bekannt sein muss, so dass er eindeutig adressiert werden
kann.

Verwendung und Änderung der I²C-Slave_Adresse

Die I²C-Slave-Adresse ist in der Firmware fest auf 64 (40Hex) gesetzt. Sie kann
jedoch im EEPROM des Geräts auf einen anderen Wert gesetzt werden.

Die Entscheidung, welche Adresse gültig ist, wird getroffen, indem ein
Options-Jumper (Options-Kurzschlussbrücke) des WS2812B-I²C-Slave (siehe Anhang
Platinenlayout) entweder kurzgeschlossen oder offen gelassen wird.

Wenn der Options-Jumper offen ist, dann wird der Festwert aus der Firmware als
I²C-Slave-Adresse verwendet. Wenn der Options-Jumper kurzgeschlossen ist, dann
wird die I²C-Slave-Adresse vom EEPROM genommen, siehe Anhang Options-Jumper.

Achtung: der Options-Jumper wird nur beim Einschalten geprüft.

LED-Steuerung

Das Gerät enthält ein Array von 52 Registern zur Steuerung der LEDs und um mit
dem Host-Controller zu kommunizieren. Jedes Register besteht aus einem Byte,
dessen Inhalt als vorzeichenlose Ganzzahl interpretiert wird.

┌───┬─────────┬───────────────────────────────────────────────────────────────┐
│Idx│Name     │Beschreibung                                                   │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 0 │REG_CTRL │Steuerungsregister                                             │
├───┴─────────┴───────────────────────────────────────────────────────────────┤
├───┬─────────┬───────────────────────────────────────────────────────────────┤
│ 1 │REG_GLB_G│Grünfarbanteil global für alle LEDs, wenn das                  │
│   │         │Steuerungsregister „Globale Farbe” fordert                     │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 2 │REG_GLB_R│Rotfarbanteil global für alle LEDs, wenn das Steuerungsregister│
│   │         │„Globale Farbe” fordert                                        │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 3 │REG_GLB_B│Blaufarbanteil global für alle LEDs, wenn das                  │
│   │         │Steuerungsregister „Globale Farbe” fordert                     │
├───┴─────────┴───────────────────────────────────────────────────────────────┤
├───┬─────────┬───────────────────────────────────────────────────────────────┤
│ 4 │LED_1_G  │Individueller Grünfarbanteil für LED 1, wenn das               │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 5 │LED_1_R  │Individueller Rotfarbanteil für LED 1, wenn das                │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 6 │LED_1_B  │Individueller Blaufarbanteil für LED 1, wenn das               │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 7 │LED_2_G  │Individueller Grünfarbanteil für LED 2, wenn das               │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 8 │LED_2_R  │Individueller Rotfarbanteil für LED 2, wenn das                │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│ 9 │LED_2_B  │Individueller Bluefarbanteil für LED 2, wenn das               │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┴─────────┴───────────────────────────────────────────────────────────────┤
│                                    . . .                                    │
├───┬─────────┬───────────────────────────────────────────────────────────────┤
│49 │LED_16_G │Individueller Grünfarbanteil für LED 16, wenn das              │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│50 │LED_16_R │Individueller Rotfarbanteil für LED 16, wenn das               │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
├───┼─────────┼───────────────────────────────────────────────────────────────┤
│51 │LED_16_B │Individueller Bluefarbanteil für LED 16, wenn das              │
│   │         │Steuerungsregister „Individuelle Farbe” fordert                │
└───┴─────────┴───────────────────────────────────────────────────────────────┘

Das Steuerungsregister REG_CTRL definiert, wie der Inhalt der anderen Register
hinsichtlich Steuerung der 16 angeschlossenen LEDs interpretiert werden soll.

Individuelle LED-Steuerung

Wenn REG_CTRL den Wert CTRL_INDIVIDUAL=0 enthält, dann werden die LEDs
individuell durch die Farbwerte in den Registern 4 („LED_1_G”) bis 51
(„LED_16_B”) gesteuert. Diese Register sind immer zu dritt gruppiert. Jede
Gruppe steuert dabei eine LED. Register 4, 5, 6 steuern LED 1, Register 7, 8, 9
steuern LED 2, und so weiter. Das erste Register jeder Gruppe („*_G”) enthält
immer den Grünfarbanteil, das zweite („*_R”) den Rotfarbanteil und das dritte
(„*_B”) den Blaufarbanteil, jeweils in Stufen von 0 bis 255.

Die Register der Indices 1, 2 und 3 („REG_GLB_G”, „REG_GLB_R”, „REG_GLB_B”)
werden in diesem Modus ignoriert.

Globale LED-Steuerung

Wenn REG_CTRL den Wert CTRL_GLB=2 enthält, dann erhalten alle LEDs die gleiche
Farbe. Diese Farbe ist definiert durch die Register 1 („REG_GLB_G”:
Grünfarbanteil), 2 („REG_GLB_R”: Rotfarbanteil) und 3 („REG_GLB_B”:
Blaufarbanteil). Register 4 bis 51 werden in diesem Fall ignoriert.

Wirbelanzeige

Wenn REG_CTRL den Wert CTRL_SWIRLY=3 enthält, dann stellen die LEDs eine Art
Farbwirbel (englisch: „Swirl”) dar, vorausgesetzt sie sind kreisförmig
angeordnet. Die Farbe wird dabei durch die Register 1, 2 und 3 bestimmt.
Register 4 bis 51 werden in diesem Fall ignoriert.

Reset-Kommando

Wenn REG_CTRL den Wert CTRL_RST=1 enthält, dann werden alle Register für
individuelle Ansteuerung (4 bis 51) auf einen Farbwert für schwach blau gesetzt
und diese Werte werden an die LEDs gesendet. Die globalen Farbwert-Registers (1
to 3) werden auf helle rote Farbe gesetzt. Diese Farbe ist nicht sofort aktiv,
sondern erst, wenn später REG_CTRL auf CTRL_GLB=1 gesetzt wird..

Danach wird REG_CTRL auf CTRL_INDIVIDUAL=0 gesetzt.

Initialisierungszustand

Wenn der WS2812B-I²C-Slave eingeschaltet wird, dann wird er intern
zurückgesetzt („Reset”), als ob er, wie im Abschnitt „Reset-Kommando”
beschrieben, ein Reset-Kommando empfangen hätte.

Kommunikation

Das Gerät erlaubt Zugriff auf die in den vorigen Abschnitten beschriebenen
Register („LED-Steuerung”) über einen I²C-Bus. Das erste Byte in jeder
Botschaft, die vom Gerät empfangen wird, enthält den Index des ersten Registers
von denen, in die die Werte der Botschaft geschrieben werden sollen. Der erste
übertragene Wert (nach dem Index) wird in dieses Register geschrieben, die
weiteren Bytes in die darauf folgenden Register, solange Bytes in der Botschaft
vorhanden sind.

Die Farbwerte im Register-Array werden via WS2812B-Bus zu den LEDs gesendet,
unmittelbar nachdem eine I²C-Botschaft vollständig und erfolgreich empfangen
worden ist, entsprechend dem aktuellen Kommando im Steuerungsregister.

Die Übertragung auf dem WS2812B-Bus benötigt etwa 630 ns.

Der WS2812B-I²C-Slave kann während dieser Zeit keine weiteren I²C-Botschaften
empfangen. Er signalisiert dem Bus-Master jedoch entsprechend dem definierten
I²C-Protokoll, dass die Übertragung weiterer Nachrichten pausieren muss, bis er
wieder zum Empfang bereit ist.

Die Registeradresse, bei der angefangen werden soll, in das Register-Array zu
schreiben, wird als erstes Byte in jeder Botschaft übertragen. Das has zwei
Vorteile:

  ● Es gibt I²C-Master-Software, die nur I²C-Botschaften mit einer begrenzten
    Länge senden kann. Die Wire-Library für Arduino [RfArduinoWire] zum
    Beispiel kann Botschaften nur bis zu einer Länge von 32 Byte senden. Mit
    Hilfe der Registeradresse im ersten Byte einer Botschaft kann man den
    vollständigen Inhalt des Register-Arrays mit mit zwei I²C-Botschaften in
    zwei Blöcken übertragen, siehe „Beispiel 6: Übertragung des Register-Arrays
    in zwei Blöcken”.

  ● Es ist nicht notwendig immer den kompletten Inhalt des Register-Arrays zu
    senden, wenn nur der Inhalt einiger weniger Register geändert werden soll.

Beispiele

Beispiel 1: LEDs global ansteuern

Die aus den folgenden Bytes bestehende Botschaft

0, 2, 0, 255, 255

startet das Schreiben in das Register-Array bei Register 0 („REG_CTRL"), zu
erkennen daran, dass das erste Byte der Botschaft 0 ist. Das zweite Byte („2”)
wird daher nach REG_CTRL geschrieben. Das dritte Byte der Botschaft („0”) wird
in das Register 1 geschrieben ("REG_GLB_G), das vierte und fünfte Byte („255”)
in die Register 2 and 3 („REG_GLB_R” and „REG_GLB_B”).

Der Wert im Steuerungsregister „REG_CTRL” wird nun als Kommando „CTRL_GLB”
interpretiert (siehe Abschnitt „Global LED Control”). Alle LEDs werden demnach
auf Grünfarbanteil 0, Rot- und Blaufarbanteil volle Stärke gesetzt, mit dem
Ergebnis von hellem Lila.

Beispiel 2: Das Gerät initialisieren

Die Botschaft zum Reset des WS2812B-I²C-Slave ist

0, 1

0 als erstes Byte zeigt an, dass nach Index 0 des Register-Arrays geschrieben
werden soll, d.h. nach REG_CTRL. Das nächste Byte („1”) wird also in REG_CTRL
geschrieben. Dort wird es, wie in „Reset-Kommando” beschrieben, als
Reset-Kommando interpretiert. Der Rest des Register-Arrays wird durch die
Botschaft selbst nicht angetastet.

Beispiel 3: Ändern einer Farbe bei globaler Steuerung

Angenommen die Steuerung wurde, wie in Beispiel „Beispiel 1: LEDs global
ansteuern” demonstriert, auf globale Steuerung gestellt, dann können zum
Beispiel die Grün- und Rotfarbanteile geändert werden, indem neue Werte nur für
die Register REG_GLB_G (Index 1) und REG_GLB_R (Index 2) gesendet werden. Wenn
der Grünanteil auf halbe Helligkeit und der Rotanteil auf 0 gesetzt werden
sollen, dann sollte die entsprechende Botschaft etwa so aussehen:

1, 127, 0

Das erste Byte („1”) zeigt an, dass ab Index 1 in das Register-Array
geschrieben werden soll, d.h. REG_GLB_G. Das zweite Byte („127”) wird also nach
REG_GLB_G geschrieben, wodurch der Grünanteil für die globale Farbeinstellung
auf 127 gesetzt wird. Das dritte Byte („0”) wird in das darauf folgende
Register (Index 2, d.h. REG_GLB_R) geschrieben, womit der Rotanteil der
globalen Farbeinstellung auf 0 gesetzt wird.

Der globale Blaufarbanteil (REG_GLB_B) wird nicht geändert, da das
Register-Array ab Index 3 (REG_GLB_B) nicht angetastet wird.

Beispiel 4: LEDs individuell ansteuern

Die folgende Beispiel-Botschaft sendet das gesamte Register-Array in einer
Botschaft (siehe „Beispiel 6: Übertragung des Register-Arrays in zwei Blöcken”,
falls die Botschaft in zwei Blöcke aufgeteilt werden muss).

0, 0, 0, 0, 0,
255, 255,   0,
255, 255,   0,
255, 255,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0

Wie der Wert des ersten Bytes („0”) anzeigt, beginnt das Schreiben bei Index 0.
Das zweite Byte („0”) wird daher nach REG_CTRL geschrieben und dort als
CTRL_INDIVIDUAL interpretiert. Dies führt, wie in Abschnitt „Individuelle
LED-Steuerung” beschrieben, zum individuellen Steuerungsmodus.

Die nächsten drei Bytes (ebenfalls alle „0”) werden zu den Indices 1, 2 und 3
(REG_GLB_G, REG_GLB_R, REG_GLB_B) geschrieben, dort jedoch ignoriert, weil das
Steuerungsregister auf individuelle Steuerung gesetzt worden ist.

Danach folgen 3·16 = 48 Bytes, die die individuellen Farbwerte für die 16 LEDs
festlegen, jeweils in 16 Gruppen zu je 3 Farbwerten, wie in Abschnitt „
Individuelle LED-Steuerung”. In dem Beispiel sind die ersten 3 LEDs auf Gelb in
voller Stärke eingestellt (Grün und Rot jeweils auf Maximum=255, Blau auf null)
und bei den übrigen 13 LEDs sind alle Werte null, d.h. die LEDs aus.

Beispiel 5: Ändern einer einzelnen LED bei individueller Steuerung

Angenommen, die Steuerung wurde auf individuelle Steuerung gestellt und alle
LEDs entsprechend eingestellt, wie in „Beispiel 4: LEDs individuell ansteuern”
gezeigt, dann genügt nur eine kurze Botschaft, um einzelne Farbwerte zu ändern.

Um in dem erwähnten Beispiel die vierte LED zusätzlich in pink leuchten zu
lassen, sollte die folgende Botschaft ausreichen:

15, 255, 255

15 als erstes Byte adressiert das Register für den Rotanteil der vierten LED
(„LED_4_R”). Die zwei Werte „255” werden also nach den Registern LED_4_R and
LED_4_B geschrieben, was dazu führt, dass die vierte LED in vollem Pink
erstrahlt.

Beispiel 6: Übertragung des Register-Arrays in zwei Blöcken

Wenn das gesamte Register-Array wie in „Beispiel 4: LEDs individuell ansteuern”
gesendet werden soll, jedoch mit einer I²C-Master-SW, die nur Botschaften mit
bis zu 32 Byte übertragen kann wie die Arduino-Wire-Library [RfArduinoWire],
dann kann die Nachricht in zwei Botschaften zu je nicht mehr als 32 Byte Länge
aufgesplittet werden.

Der erste Block enthält genau 32 Bytes, d.h. 31 Registerwerte plus die
Register-Adresse (mit „0”) als erstes Byte:

0, 0, 0, 0, 0,
255, 255,   0,
255, 255,   0,
255, 255,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0

Der zweite Block enthält die übrigen 21 Registerwerte plus ebenfalls am Anfang
die Register-Adresse, diesmal mit „31” — dem Index, der auf die vorher
übertragene Botschaft folgt:

 31,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0,
  0,   0,   0

Stromversorgung

Der WS2812B-I²C-Slave und die LEDs werden normalerweise über die
Stromversorgungs-Pins des Geräts („+5V”) versorgt, siehe Bild 1 und Anhang „
Platinenlayout”.

Das System kann in diesem Fall mit einer Spannung entsprechend der
WS2812B-Spezifikation [RfWS2812B], gewöhnlich 5 V Gleichspannung. Der ganze
Spannungsbereich ist 3,7 V bis 5,3 V.

Der Stromverbrauch des WS2812B-I²C-Slave und der 16 WS2812B-LEDs liegt zusammen
bei etwa 0,75 A bei 5 V.

Alternative Stromversorgung

Figure 2: Alternative Power Supply

Bild 2: Alternative Stromversorgung

Es ist jedoch möglich, die Stromversorgungen von WS2812B-I²C-Slave und LEDs zu
trennen, wie in Bild 2 zu sehen. Die Versorgungsleitung im dreiadrigen
LED-Verbindungskabel wird aufgetrennt. Das Ende auf der Seite des
WS2812B-I²C-Slave (im Bild „LED +5V”) wird offen gelassen und das Ende auf der
LED-Seite wird mit einer getrennten LED-Stromversorgung verbunden. Die Spannung
dieser separaten Versorgung muss dann im zulässigen Bereich der
WS2812B-Spezifikation von 3,7 V bis 5,3 V liegen. Der Stromverbrauch liegt bei
etw 0,75 A bei 5 V.

Die Stromversorgung des WS2812B-I²C-Slave kann nun in in einem größeren Bereich
liegen: 3,3 V bis 5,3 V. Der Stromverbrauch hier ist dann etwa 8 mA bei 5 V und
3 mA at 3,3 V. Die Spannung kann eigentlich bis auf 1,8 V sinken, siehe Anhang
„Technische Spezifikation”. Der Ausgang kann dann jedoch die WS2812B-LEDs ohne
einen zusätzlich Pegelwandler nicht mehr ansteuern.

Aktualisierung der Firmware

Es ist möglich eine neue Firmware in das Gerät zu laden. Dazu wird ein
ISP-Programmer benötigt (ISP: In-System Programming, manchmal auch In-circuit
Serial Programming, siehe Wikipedia: In-System-Programmierung [RfWPISP_de]).
Ein Arduino kann als ISP-Programmer verwendet werden, siehe „Randall Bohn:
ArduinoISP” [RfArduinoISP]. Erläuterungen dazu liefert ein Tutorium (in
English) auf der Arduino Web-Seite „Tutorial: Arduino as ISP and Arduino
Bootloaders” [RfArduinoISPTut].

Außerdem ist ein Upload-Programm erforderlich, um den Programmer zu steuern.
avrdude „AVR Downloader/UploaDEr - Summary”[Rfavrdude] ist ein Programm, das
für diesen Zweck verwendet werden kann, sowohl unter Linux als auch unter
Windows.

Außerdem wird natürlich die neue Firmware in Form einer Intel-Hex-Datei
benötigt.

Ablauf der Firmwareaktualisierung

Für die Aktualisierung der Firmware muss ein bestimmter Ablauf eingehalten
werden.

 1. Ausschalten oder Trennen der Stromversorgung des WS2812B-I²C-Slave.

 2. Trennung des I²C-Steckers.

 3. Verbinden des ISP-Programmers mit dem Host-PC (normalerweise über USB).

 4. Verbinden des ISP-Programmers mit dem WS2812B-I²C-Slave mit einem
    ISP-Kabel, siehe Anhang „ISP-Anschluss J4”. Prüfen Sie, ob der Stecker
    richtig herum sitzt. Er könnte um 180° verdreht aufgesteckt sein, was zu
    einer Beschädigung des Geräts oder des Programmers führen könnte.

 5. Prüfem ob der Programmer das gleiche Spannungsniveau verwendet, wie der
    WS2812B-I²C-Slave. Dies ist meistens 5 V. Es ist jedoch möglich den
    WS2812B-I²C-Slave mit einer niedrigeren Spannung zu versorgen, siehe
    Abschnitt „Alternative Stromversorgung”. Wenn der ISP-Programmer 5 V bei
    der Programmierung des WS2812B-I²C-Slave verwendet, während dieser mit
    3,3 V versorgt wird, kann dieser beschädigt werden. Der Programmer muss auf
    das Spannungsniveau des WS2812B-I²C-Slave eingestellt werden, oder es muss
    ein entsprechender Pegelwandler verwendet werden.

 6. Verbinden und Einschalten der Stromversorgung des WS2812B-I²C-Slave.

 7. Starten des Upload-Programms auf dem Host-PC. Bzgl. Details siehe „
    Firmwareaktualisierung bei Linux” bzw. „Firmwareaktualisierung bei MS
    Windows”.

 8. Falls erforderlich Programmieren der optionalen I²C-Slave-Adresse in das
    EEPROM des Geräts. Details siehe Abschnitt „Änderung der optionalen
    Slave-Adresse im EEPROM”. Dieser Schritt ist natürlich nicht notwendig,
    wenn die optionale I²C-Slave-Adresse im EEPROM nicht verwendet wird, siehe
    Abschnitt „Verwendung und Änderung der I²C-Slave_Adresse”.

    Dieser Schritt ist immer nötig (falls die optionale I²C-Slave-Adresse
    verwendet wird), wenn eine neue Firmware in das Gerät geladen wurde, auch
    wenn die I²C-Slave-Adresse nicht geändert werden soll, weil das EEPROM beim
    Laden einer Firmware jedesmal gelöscht wird.

 9. Es kann vorkommen, dass der WS2812B-I²C-Slave am Ende zurückgesetzt werden
    muss, z.B. durch Aus- und Wiedereinschalten.

Firmwareaktualisierung bei Linux

Wenn Sie avrdude verwenden, müssen Sie zunächst den Programmer mit dem Linux-PC
verbinden, normalerweise über USB.

Auf Linux-Systemen mit Kernel-Versionen von 2.6 an ist es ziemlich einfach eine
Kommunikation mit dem Programmer herzustellen, da alle erforderlichenTreiber
bereits an Bord sein sollten. Die Kernel-Version kann man in einer
Command-Line-Shell mit diesem Kommando ermitteln:

uname -a

Nachdem der Programmer über USB mit dem PC verbunden wurde, erzeugt das
Betriebssystem ein neues Interface-Device. avrdude benötigt den Namen dieses
Devices. Im Fall von USB kann er in einer Command-Line-Shell mit diesem
Kommando ermittelt werden:

ls -lrt /dev/tty*

Dieses Kommando listet alle seriellen Devices in der Reihenfolge ihrer
Erstellung auf, das zuletzt erzeugte als letztes. Wenn das Kommando unmittelbar
nach dem Verbinden des Programmers eingegeben wurde, wird das Programmer-Device
als letztes aufgelistet. Das Ergebnis des Kommandos sollte etwa so aussehen:

...
crw--w---- 1 root tty       4, 60 May 15 08:12 /dev/tty60
crw-rw---- 1 root dialout   4, 68 May 15 08:13 /dev/ttyS4
crw--w---- 1 root tty       4, 10 May 15 09:15 /dev/tty10
crw-rw---- 1 root dialout 166,  0 May 15 14:52 /dev/ttyACM0

In diesem Beispiel ist der Name des Programmer-Devices /dev/ttyACM0. Es wurde
als letztes um 14:52 Uhr erzeugt.

Die Firmware befindet sich in einer Intel-Hex-Datei, z.B. ws2812Bi2cslave.hex.

Angenommen, der Device-Name ist /dev/ttyACM0, die Firmware-Datei ist
ws2812Bi2cslave.hex und der Programmer ist ein Arduino, dann können Sie in
einer Command-Line-Shell von dem Verzeichnis aus, in dem die Firmware-Datei
liegt, den Firmware-Upload mit diesem Kommando durchführen:

avrdude -P/dev/ttyACM0 -pattiny45 -carduino -b115200\
 -Uflash:w:ws2812Bi2cslave.hex:i

Die hier verwendeten Optionen sind:

-P  Programmer Port
-p  Typ des Microcontrollers
-c  Programmer
-b  Baudrate
-U  Typ der Memory-Operation

Wenn statt einem Arduino ein anderer Programmer Verwendung findet, dann müssen
die Optionen -c und -b angepasst werden, u.U. müssen auch andere Optionen
verwendet werden. Details über die Benutzung von avrdude finden sich im
avrdude-Manual:

man avrdude

Firmwareaktualisierung bei MS Windows

Auch unter Windows kann die Firmware mit avrdude aktualisiert werden. Hinweise
dazu finden sich vermutlich auf der avrdude Web-Seite „AVR Downloader/UploaDEr
- Summary” [Rfavrdude].

Optionale EEPROM-I²C-Slave-Adresse

Änderung der optionalen Slave-Adresse im EEPROM

Jeder an einem I²C-Bus angeschlossene Slave muss eine dem Bus-Master bekannte
und für den Bus eindeutige Slave-Adresse haben.

Die Slave-Adresse des WS2812B-I²C-Slave ist in seiner Firmware fest auf 64
(40Hex) gesetzt. Es kann jedoch vorkommen, das diese Adresse schon von einem
anderen Slave benutzt wird. Für diesen Fall gibt es die Möglichkeit, die
Slave-Adresse im EEPROM des WS2812B-I²C-Slave auf einen anderen Wert zu setzen.

Wenn beim Einschalten des WS2812B-I²C-Slave der Options-Jumper J5 nach Masse
kurzgeschlossen ist, dann wird die Slave-Adresse vom EEPROM verwendet.

Diese EEPROM-Slave-Adresse kann über ISP geändert werden, ähnlich wie in
Abschnitt „Firmware Update” für das Laden einer neuen Firmware beschrieben
wird. Es wird dafür dieselbe Hard- und Software benötigt, abgesehen davon, das
keine Firmware-Intel-Hex-Datei vorhanden sein muss.

Der Upload-Ablauf ist der gleiche wie im eben erwähnten Abschnitt beschrieben.
Das Upload-Programm muss jedoch mit anderen Argumenten aufgerufen werden, zum
Beispiel wie:

avrdude -P/dev/ttyACM0 -pattiny45 -carduino -b115200 -Ueeprom:w:0xff,0x22:m

Die Optionen -P, -p, -c, -b sind dieselben, wie im Abschnitt über die
Firmware-Aktualisierung beschrieben. Der einzige Unterschied tritt bei der
Option -U auf.

Die zwei Parameter 0xff und 0x22 haben die folgende Bedeutung:

0xff

    Dies ist der Hexadezimalwert für die EEPROM-Adresse 0. Er wird nicht
    verwendet und ignoriert von der Firmware. 0xff ist der Hexadezimalwert
    einer löschten EEPROM-Zelle mit vorangestelltem 0x. Jeder andere Wert
    kleiner oder gleich ffHex ist jedoch ebenfalls in Ordnung.

0x22

    Dies ist der Hexadezimalwert für die EEPROM-Adresse 1. Dies ist der Wert,
    der als I²C-Slave-Address verwendet wird, wenn das Gerät mit nach Masse
    kurzgeschlossenem Options-Jumper eingeschaltet wird. In diesem Beispiel ist
    er auf 22Hex = 34 gesetzt mit vorangestelltem 0x.

Lesen der optionalen Slave-Adresse im EEPROM

Die Slave-Adresse im EEPROM kann auch zur Überprüfung ausgelesen werden. Der
Befehl dazu ist:

avrdude -P/dev/ttyACM0 -pattiny45 -carduino -b115200 -Ueeprom:r:eeprom.txt:h

Dies lädt den gesamten Inhalt des EEPROMs in die Textdatei eeprom.txt. Diese
Datei enthält die Werte aller 256 EEPROM-Zellen als Hexadezimalzahlen mit
jeweils vorangestelltem 0x. Der zweite Wert (entsprechend der EEPROM-Adresse 1)
ist die optionale I²C-Slave-Adresse. Alle anderen Werte können ignoriert
werden.

Anhang

Anerkennungen

Die Software des WS2812B-I²C-Slave ist eine substantielle Erweiterung des
neopixel_i2c_slave von Brian Starkey [RfNeoPixI2CSlave].

Sie verwendet eine leicht modifizierte Version der I2C-Slave-Library
usi_i2c_slave von Brian Starkey [RfUsiI2CSlave].

Sie verwendet außerdem einen leicht modifizierten Teil der light_ws2812 V2.3
Library von Tim (cpldcpu@gmail.com) vom 20.02.2016 [RfLightWS2812Lib].

Schaltplan

Circuit diagram of the WS2812B I²C Slave

Schaltplan des WS2812B I²C Slave

Platinenlayout

Board layout of the WS2812B I²C Slave

Platinenlayout des WS2812B I²C Slave

Steckerbelegungen

Spannungsversorgungsanschluss J1

Power connector

Der Spannungsversorgungsanschluss ist ein Zwei-Pin-Stecker. Die
Pin-Nummerierung ist:

Tabelle 1. Spannungsversorgungssteckerbelegung

┌─┬───┬────────────────────┐
│1│GND│Masse               │
├─┼───┼────────────────────┤
│2│+5V│Spannungsversorgung │
└─┴───┴────────────────────┘


I²C-Bus-Anschluss J2

I²C bus connector

Der I²C-Bus-Anschluss ist ein Drei-Pin-Stecker. Die Pin-Nummerierung ist:

Tabelle 2. I²C-Steckerbelegung

┌─┬───┬──────────┐
│1│GND│Masse     │
├─┼───┼──────────┤
│2│SDA│I²C Data  │
├─┼───┼──────────┤
│3│SCL│I²C Clock │
└─┴───┴──────────┘


WS2812B-LED-Anschluss J3

WS2812B LED connector

Der WS2812B-LED-Anschluss ist ein Drei-Pin-Stecker. Die Pin-Nummerierung ist:

Tabelle 3. WS2812B-LED-Steckerbelegung

┌─┬──────┬────────────────────────────────┐
│1│GND   │Masse                           │
├─┼──────┼────────────────────────────────┤
│2│D[OUT]│Datenausgang zu den LEDs        │
├─┼──────┼────────────────────────────────┤
│3│+5V   │Spannungsversorgung für die LEDs│
└─┴──────┴────────────────────────────────┘


ISP-Anschluss J4

ISP connector

Der ISP-Anschluss ist ein Sechs-Pin-Stecker. Die Pin-Nummerierung ist:

Tabelle 4. ISP-Steckerbelegung

┌─┬─────┬────────────────────┐
│1│MISO │Master In Slave Out │
├─┼─────┼────────────────────┤
│2│V[CC]│+5V                 │
├─┼─────┼────────────────────┤
│3│SCK  │Clock               │
├─┼─────┼────────────────────┤
│4│MOSI │Master Out Slave In │
├─┼─────┼────────────────────┤
│5│RESET│Reset               │
├─┼─────┼────────────────────┤
│6│GND  │Ground              │
└─┴─────┴────────────────────┘


Options-Jumper J5

Option Jumper

Der Options-Jumper ist ein Zwei-Pin-Stecker, der entweder offen gelassen wird
oder mit einem Kurzschlussstecker überbrückt wird.

An Hand von ihm wird die I²C-Slave-Adresse des Geräts fest gelegt.

Achtung: Der Pin wird nur beim Einschalten des Geräts geprüft.

Tabelle 5. Options-Jumper

┌───────────────┬───────────────────────────────────────────────┐
│     Offen     │Die I²C-Slave-Adresse ist fest auf 64 (40Hex). │
├───────────────┼───────────────────────────────────────────────┤
│Kurzgeschlossen│Die I²C-Slave-Adresse wird vom EEPROM genommen.│
└───────────────┴───────────────────────────────────────────────┘


Technische Daten

┌───────────────────────────────────────────┬─────────────────────────────────┐
│Spannungsversorgungsbereich                │1,8 – 6 V DC^[a] ^[b]            │
├───────────────────────────────────────────┼─────────────────────────────────┤
│Stromverbrauch (V[CC]=5V, Ausgang nicht    │                                 │
│angeschlossen, Stromverbrauch der LEDs     │max. 8 mA^[a]                    │
│nicht berücksichtigt)                      │                                 │
├───────────────────────────────────────────┼─────────────────────────────────┤
│Start-Up-Zeit                              │65 ms                            │
├───────────────────────────────────────────┴─────────────────────────────────┤
│WS2812B-Anschluss D[OUT]                                                     │
├───────────────────────────────────────────┬─────────────────────────────────┤
│Output-High-Voltage bei V[CC]=5V, I[OH]    │≥4.3 V^[a]                       │
│≥-10mA                                     │                                 │
├───────────────────────────────────────────┼─────────────────────────────────┤
│Output-Low-Voltage bei V[CC]=5V, I[OL]≤10mA│≤0.6 V^[a]                       │
├───────────────────────────────────────────┼─────────────────────────────────┤
│Ausgangsinnenwiderstand                    │120 Ω (Tol. 5%)                  │
├───────────────────────────────────────────┼─────────────────────────────────┤
│Max. Ausgangsbelastung                     │Kurzschlussfest                  │
├───────────────────────────────────────────┴─────────────────────────────────┤
│I²C-Bus-Anschluss SDA, SCL                                                   │
├───────────────────────────────────────────┬─────────────────────────────────┤
│Treibercharakteristik                      │Open-Collector, kein             │
│                                           │Pull-Up-Widerstand               │
├───────────────────────────────────────────┼─────────────────────────────────┤
│V[IL] (V[CC]=2.4V–5.5V)                    │min. -0,5V[CC], max. 0,3V[CC]^[a]│
├───────────────────────────────────────────┼─────────────────────────────────┤
│V[IH] (V[CC]=2,4V–5,5V)                    │min. 0,6V[CC], max. V[CC]+0,5V^  │
│                                           │[a]                              │
├───────────────────────────────────────────┴─────────────────────────────────┤
│Abmessungen:                                                                 │
├───────────────────────────────────────────┬─────────────────────────────────┤
│Länge × Breite × Höhe                      │30,5 × 28 × 13 mm                │
├───────────────────────────────────────────┼─────────────────────────────────┤
│                                           │2- oder 3- oder 6-Pin-Stecker    │
│                                           ├─────────────────────────────────┤
│Anschlüsse                                 │Pin-Länge            5,8 mm      │
│                                           │Pin-Abstand          2,54 mm     │
│                                           │Pin-Querschnittsform Quadratisch │
│                                           │                     0,65×0,65 mm│
├───────────────────────────────────────────┴─────────────────────────────────┤
│^[a] Wert ohne weitere Überprüfung direkt aus der Microchip-Spezifikation    │
│[RfATtiny45DS] „ATtiny45 Datasheet Rev. 2586Q–AVR–08/2013”. Keine Gewähr für │
│die Richtigkeit dieses Werts.                                                │
│                                                                             │
│^[b] Bitte beachten, dass der zulässige Spannungsbereich von angeschlossenen │
│WS2812B-LEDs gemäß dem WS2812B-Datenblatt [RfWS2812BDS] nur 3.7V–5.3V        │
│beträgt.                                                                     │
└─────────────────────────────────────────────────────────────────────────────┘

Bzgl. weiterer Spezifikationen siehe ATtiny45-Datenblatt [RfATtiny45DS].

Quellennachweis

[RfWPI2C_de] „Wikipedia: I²C“
https://de.wikipedia.org/wiki/I%C2%B2C (retrieved 2020-Oct-25^th)

[RfATtiny45DS] „ATtiny45 Datasheet Rev. 2586Q–AVR–08/2013“
http://ww1.microchip.com/downloads/en/DeviceDoc/
Atmel-2586-AVR-8-bit-Microcontroller-ATtiny25-ATtiny45-ATtiny85_Datasheet.pdf
(retrieved 2020-Oct-25^th)

[Rfavrdude] „AVR Downloader/UploaDEr - Summary“
http://savannah.nongnu.org/projects/avrdude (retrieved 2020-Oct-25^th)

[RfWPISP_de] „Wikipedia: In-System-Programmierung“
https://de.wikipedia.org/wiki/In-System-Programmierung (retrieved 2020-Oct-25^
th)

[RfWS2812B] „Worldsemi Introduce WS2812B“
http://www.world-semi.com/Certifications/WS2812B.html (retrieved 2020-Oct-25^
th)

[RfWS2812BDS] „WS2812B Intelligent control LED integrated light source, Ver.
No.: V5“
http://www.world-semi.com/DownLoadFile/108 (retrieved 2020-Oct-25^th)

[RfArduinoWire] „Arduino Reference|Language|Libraries Wire Library“
https://www.arduino.cc/en/Reference/Wire (retrieved 2020-Oct-25^th)

[RfArduinoISP] „Randall Bohn: ArduinoISP“
https://github.com/rsbohn/ArduinoISP (retrieved 2020-Oct-25^th)

[RfArduinoISPTut] „Tutorial: Arduino as ISP and Arduino Bootloaders“
https://www.arduino.cc/en/Tutorial/BuiltInExamples/ArduinoISP (retrieved
2020-Oct-25^th)

[RfNeoPixI2CSlave] „neopixel_i2c_slave (picopixel)“
https://github.com/usedbytes/neopixel_i2c (retrieved 2020-Oct-25^th)

[RfUsiI2CSlave] „neopixel_i2c_slave (picopixel)“
https://github.com/usedbytes/usi_i2c_slave (retrieved 2020-Oct-25^th)

[RfLightWS2812Lib] „light_ws2812 V2.3“
https://github.com/usedbytes/light_ws2812/tree/
b4dcd56029ee2d20913c29acfc43fc05904f9271 (retrieved 2020-Oct-25^th)

