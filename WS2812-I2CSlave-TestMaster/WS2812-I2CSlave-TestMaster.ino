//------------------------------------------------------------------------------
//
// File WS2812-I2CSlave-TestMaster.ino
//
// Test Program to test a WS2812B IC2 slave from an Arduino as an I2C master
//
// Version 1.0   2020 Oct 25th   jost.brachert@gmx.de
//
// Copyright (C) 2020  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the license, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//
//------------------------------------------------------------------------------
// The program uses the Wire library to implement the I2C interface as a master.
// NOTE however: The size of the internal buffers of the Wire library must be
// increased from 32 bytes to 64 bytes.
// For the version 1.0 of the library (retrieved 2017-Oct-02) for example that
// can be achieved by changing constant TWI_BUFFER_LENGTH in file
// <Arduino directory>/hardware/arduino/avr/libraries/Wire/src/utility/twi.h
// from 32 to 64 and constant BUFFER_LENGTH in file
// <Arduino directory>/hardware/arduino/avr/libraries/Wire/src/Wire.h
// from 32 to 64.
// This hint is given WITHOUT ANY WARRANTY. If you do that change you do that
// at your own risk.

#include <stdint.h>         // uint8_t
#include <Arduino.h>        // Serial,Wire,pinMode(),digitalWrite(),delay()
                            // OUTPUT,HIGH,LOW
#include <Wire.h>           // NOTE: Change buffer size to 64 before using it

// NOTE that this file must be identical to the same file used at compilation
// of the WS2812B IC2 Slave:
#include "i2c_slave_defs.h" // I2C_N_REG,REG_CTRL,CTRL_RST,CTRL_GLB,CTRL_SWIRLY
                            // I2C_N_GLB_REG,N_LEDS,I2C_SLAVE_ADDR

// All sizes are one byte more as in the slave to consider the address register
#define I2CMSR_N_REG_CTRL (1+1)             // Bytes to transfer the reset cmd
#define I2CMSR_N_GLB_REG  (1+I2C_N_GLB_REG) // Bytes for REG_GLB command
#define I2CMSR_N_REG      (1+I2C_N_REG)     // Total bytes to be transmitted

#define MSR_REG_ADDR  (i2c_reg[0])          // First transmitted byte is
                                            //   the register address
#define MSR_REG_CTRL  (*(&REG_CTRL+1))      // All other registers shifted by
#define MSR_REG_GLB_G (*(&REG_GLB_G+1))     //   one byte because of the
#define MSR_REG_GLB_R (*(&REG_GLB_R+1))     //   additional register address
#define MSR_REG_GLB_B (*(&REG_GLB_B+1))     //   byte

static const uint8_t  ledPin = 13;

static uint8_t  i2c_reg[I2CMSR_N_REG];      // Array to be transmitted to the
                                            //   slave

// If defined will take the I2C slave address from i2c_slave_defs.h:
#define DEFAULT_SLAVE_ADDR
#ifdef DEFAULT_SLAVE_ADDR
static uint8_t  ic2_slaveAddr = I2C_SLAVE_ADDR;
#else
static uint8_t  ic2_slaveAddr = 0x22;
#endif


//==============================================================================
// Initialization

void setup () {
    pinMode (ledPin, OUTPUT);
    Serial.begin (115200);
    Wire.begin ();                  // Start the I2C bus as master
}



//==============================================================================
// Endless Main Loop

void loop () {
#if 0
    //--------------------------------------------------------------------------
    // Reset

    MSR_REG_ADDR = 0;                           // Register address in the slave
    MSR_REG_CTRL = CTRL_RST;
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_REG_CTRL);    // Send data
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // Set all LEDs to the default color

    MSR_REG_ADDR = 0;               // In the slave start at register address 0
    MSR_REG_CTRL = CTRL_GLB;        // Set LEDs all the same
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_REG_CTRL);    // Send data
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // Set all LEDs to the same color and brightness

    MSR_REG_ADDR  =  0;             // In the slave start at register address 0
    MSR_REG_CTRL  = CTRL_GLB;       // Set LEDs all the same
    MSR_REG_GLB_G = 50;             // Green color value
    MSR_REG_GLB_R =  0;             // Red color value
    MSR_REG_GLB_B = 50;             // Blue color value
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_GLB_REG);     // Send data
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // Set all LEDs to the default global value

    // Reset slave to load the default values
    MSR_REG_ADDR =  0;              // In the slave start at register address 0
    MSR_REG_CTRL = CTRL_RST;
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_REG_CTRL);    // Send reset command
    Wire.endTransmission ();                    // Stop transmitting

    // Let the default LED values be displayed
    MSR_REG_CTRL = CTRL_GLB;
    Wire.beginTransmission (ic2_slaveAddr);
    Wire.write (i2c_reg, I2CMSR_N_REG_CTRL);    // Send reset command
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // One color with increasing intensity for increasing LED number.
    // First LED with additional red and last one with additional blue color.
    uint8_t  s = I2CMSR_N_GLB_REG;
    uint8_t  k = 10;
    for (uint8_t i=s; i<I2CMSR_N_REG-2; i+=3) {
        i2c_reg[i  ] = k;
        i2c_reg[i+1] = 0;
        i2c_reg[i+2] = 0;
        k += 10;
    }
    i2c_reg[s+3* 0+1] = 255;
    i2c_reg[s+3*15+2] = 255;
    MSR_REG_ADDR = 0;               // In the slave start at register address 0
    MSR_REG_CTRL = CTRL_INDIVIDUAL; // Default action: Individual LED control
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_REG);         // Send data
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // One color with increasing intensity for increasing LED number.
    // First LED with additional red and last one with additional blue color.
    //
    // Transmission in two chunks.

    uint8_t  s = I2CMSR_N_GLB_REG;
    uint8_t  k = 10;
    for (uint8_t i=s; i<I2CMSR_N_REG-2; i+=3) {
        i2c_reg[i  ] = k;
        i2c_reg[i+1] = 0;
        i2c_reg[i+2] = k;
        k += 10;
    }
    i2c_reg[s+3* 0+1] = 255;
    i2c_reg[s+3*15+2] = 255;
    MSR_REG_CTRL = CTRL_INDIVIDUAL; // Default action: Individual LED control

#define CHUNKSIZE   32
    // Transmit first chunk to device at address ic2_slaveAddr
    MSR_REG_ADDR = 0;               // In the slave start at register address 0
    Wire.beginTransmission (ic2_slaveAddr);     // Initiate transmission
    Wire.write (i2c_reg, CHUNKSIZE);            // Send data 1st chunk
    Wire.endTransmission ();                    // Stop transmitting

    // The Slave starts driving the LEDs immediately after the I2C message has
    // been received. Driving the LEDs lasts about 630 ns. The slave is not able
    // to receive a new message during that time. It will stretch the I2C clock
    // in that case until the LED driving sequence is finished to signal to the
    // I2C master that it has to pause the transmission according to the IC2
    // protocol specification. This is handled transparently inside the IC2
    // handler. It is therefore safe to let the next transmission command follow
    // immediately. However, there is time of 650 us between the transmissions
    // to do other things.
    delayMicroseconds (650);

    // Transmit second chunk to device at address ic2_slaveAddr
    MSR_REG_ADDR = CHUNKSIZE - 1;       // Subtract 1 because the addr.register
                                        // doesn't count in the slave
    Wire.beginTransmission (ic2_slaveAddr);     // Initiate transmission
    Wire.write (MSR_REG_ADDR);                  // Send address register only
    Wire.write (i2c_reg+CHUNKSIZE, I2CMSR_N_REG-CHUNKSIZE);     // Send rest
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // Individual dynamic LED control

    static uint16_t  cnt;

    MSR_REG_ADDR = 0;               // In the slave start at register address 0
    MSR_REG_CTRL = CTRL_INDIVIDUAL; // Default action: Individual LED control

    uint8_t  iLED = 0;
    for (uint8_t i=0; i<I2CMSR_N_REG-2; i+=3,++iLED) {
        i2c_reg[(I2CMSR_N_GLB_REG +0) + i] = ((iLED+ 0+cnt)%N_LEDS)*255u/N_LEDS/2u;
        i2c_reg[(I2CMSR_N_GLB_REG +1) + i] = ((iLED+ 5+cnt)%N_LEDS)*255u/N_LEDS/2u;
        i2c_reg[(I2CMSR_N_GLB_REG +2) + i] = ((iLED+10+cnt)%N_LEDS)*255u/N_LEDS/2u;
    }
    if (++cnt >= N_LEDS)  cnt = 0;
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_REG);         // Send data
    Wire.endTransmission ();                    // Stop transmitting
#endif

#if 0
    //--------------------------------------------------------------------------
    // Blink a single LED

    static uint16_t  cnt;

    // Initialize the slave
    if (cnt == 0) {
        uint8_t  s = I2CMSR_N_GLB_REG;
        uint8_t  k = 10;
        for (uint8_t i=s; i<I2CMSR_N_REG-2; i+=3) {
            i2c_reg[i  ] = k;
            i2c_reg[i+1] = 0;
            i2c_reg[i+2] = 0;
            k += 10;
        }
        i2c_reg[s+3* 0+1] = 255;
        i2c_reg[s+3*15+2] = 255;
        MSR_REG_ADDR = 0;                  // In the slave start at reg. addr. 0
        MSR_REG_CTRL = CTRL_INDIVIDUAL;    // Individual LED control
                                                    // Transmit to device at
        Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
        Wire.write (i2c_reg, I2CMSR_N_REG);         // Send data
        Wire.endTransmission ();                    // Stop transmitting
    } else {
        static const uint8_t  iLed = 5;     // Index of the LED to blink
        static uint8_t  i2cArray[4] = {     // Array to send to I2C slave
            I2C_N_GLB_REG + iLed*3,         // Register address of the LED
            0, 0, 0};                       // Color
        Wire.beginTransmission (ic2_slaveAddr);
        i2cArray[1] = (cnt & 1) ? 0 : iLed*10;
        Wire.write (i2cArray, 4);
        Wire.endTransmission ();
    }
    ++cnt;
#endif

#if 1
    //--------------------------------------------------------------------------
    // Swirly

    MSR_REG_ADDR  =   0;            // In the slave start at register address 0
    MSR_REG_CTRL  = CTRL_SWIRLY;    // Set Swirly command
    MSR_REG_GLB_G =   0;            // Green color value
    MSR_REG_GLB_R = 255;            // Red color value
    MSR_REG_GLB_B = 200;            // Blue color value
                                                // Transmit to device at
    Wire.beginTransmission (ic2_slaveAddr);     //   address ic2_slaveAddr
    Wire.write (i2c_reg, I2CMSR_N_GLB_REG);     // Send data
    Wire.endTransmission ();                    // Stop transmitting
    delay (10000);
#endif

    digitalWrite (ledPin, HIGH);
    delay (100);
    digitalWrite (ledPin, LOW);
//    delay (400);
    delay (900);
}

